FROM archlinux

## Tell systemd we are running in container
ENV container=docker

## Use system target with minimal dependencies
COPY container.target /etc/systemd/system/container.target

## Do not remove "iproute2", it is required to make {{ ansible_interfaces }} work
RUN pacman -Syu --noconfirm python iproute2 && yes | pacman -Scc && \
    ln -sf /etc/systemd/system/container.target /etc/systemd/system/default.target && \
    ln -sf /lib/systemd/systemd /sbin/init

## Run systemd by default
ENTRYPOINT ["/sbin/init"]

CMD ["--log-level=info"]

## This makes systemd correctly respond on 'docker stop' command
STOPSIGNAL SIGRTMIN+3
